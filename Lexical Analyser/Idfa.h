#pragma once

#include <string>
#include <vector>
#include <utility>
#include "Node.h"

//Base class for each Dfa it will define common function and variables
class Idfa
{
public:
	Idfa(std::string);
	~Idfa();
	std::pair<bool, bool> Check_node(Node*, std::string, bool);
	void Match(std::string);

	bool is_matching;
	bool is_end;
	std::string token_name;
	std::string id;
	Node *head;
};

