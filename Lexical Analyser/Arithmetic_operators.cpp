#include "Arithmetic_operators.h"

//Creation of the DFA Graph of the ARITHMETIC OPERATOR token
Arithmetic_operators::Arithmetic_operators() : Idfa("ARITHMETIC OPERATOR") {
	head = new Node();

	auto q1 = new Node(true, "addsub");
	
	auto q2 = new Node(true, "addsub");
	
	auto q3 = new Node(true, "multdiv");
	
	auto q4 = new Node(true, "multdiv");

	head->addPair("+", q1);

	head->addPair("-", q2);
	
	head->addPair("*", q3);
	
	head->addPair("/", q4);
}

Arithmetic_operators::~Arithmetic_operators() {}