#include "Expr_string.h"

//Creation of the DFA Graph of the EXPR STRING token
Expr_string::Expr_string() : Idfa("EXPR STRING") {
	head = new Node();
	auto q1 = new Node();
	auto q2 = new Node(true, "vtype");

	head->addPair("\"", q1);
	q1->addPair("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ", q1);
	q1->addPair("\"", q2);
}

Expr_string::~Expr_string() {}