#include <iostream>
#include "Idfa.h"

Idfa::Idfa(std::string _id) : is_matching(false), is_end(false), id(_id), head()
{
}

Idfa::~Idfa()
{
}

//the recursive function checking the graph
std::pair<bool, bool> Idfa::Check_node(Node* node, std::string in, bool match)
{
	bool end = node->end;
	//if we reached one of the end of the graph we store the name of the token name
	if (end)
		token_name = node->name;

	//we try to see if any of the node attached to our current node matches with the current character in the string
	for (auto link : node->links) {
		if (link.first.find(in[0]) != std::string::npos) {
			//if we still have characters in out string we go to the next node and next character in our string
			if (in[1])
				return Check_node(link.second, &in[1], false);
			end = link.second->end;
			if (end)
				token_name = link.second->name;
			match = true;
			break;
		}
		else
			match = false;
	}
	//if the string has still characters and we reaching an end node then it's not a total match for example "inti" shouldn't match "int"
	if (in[1])
		match = false;
	return std::make_pair(end, match);
}

//calls the recursive Check_node function that will go through the dfa's nodes
void Idfa::Match(std::string in)
{
	auto ret = Check_node(head, in, false);

	is_end = ret.first;
	is_matching = ret.second;
}
