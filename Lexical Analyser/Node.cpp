#include "Node.h"

Node::Node() : links(), end(false)
{
}

Node::Node(bool _end, std::string _name) : links(), end(_end), name(_name)
{
}

Node::~Node()
{
}

void Node::addPair(std::string str, Node *node) {
	links.push_back(std::make_pair(str, node));
}