#include "Lexical_analyser.h"

//class constructor that will add every token we want to reconise to it's vector
//also it will open the input file
Lexical_analyser::Lexical_analyser(std::string _file) : file(std::ifstream(_file)) {
	expr.push_back(*new Definition_symbols());
	expr.push_back(*new Indication_symbols());
	expr.push_back(*new Variable_type());
	expr.push_back(*new Keyword());
	expr.push_back(*new Boolean_string());
	expr.push_back(*new Float_number());
	expr.push_back(*new Signed_int());
	expr.push_back(*new Arithmetic_operators());
	expr.push_back(*new Bitwise_operators());
	expr.push_back(*new Assignment_operator());
	expr.push_back(*new Comparison_operators());
	expr.push_back(*new Term_statement());
	expr.push_back(*new Input_separator());
	expr.push_back(*new Expr_string());
	expr.push_back(*new Identifier());
	expr.push_back(*new Whitespaces());
	if (!file) {
		std::cout << "couldn't open file" << std::endl;
	}
}

Lexical_analyser::~Lexical_analyser() {}

void Lexical_analyser::Open_file(std::string _file) {
	file = std::ifstream(_file);
	if (!file) {
		std::cout << "couldn't open file" << std::endl;
	}
}

//Function that will perform the reading and analyse of the input file, while writing symbols into the buffer string
int Lexical_analyser::Analyse() {
	//Will read the file character by character
	while (file >> std::noskipws >> c) {
		// add the new character read to the string
		str += c;
		rematch:
		matched = 0;
		curr = 0;
		// this loop will test the current string "str" with each token in our vector
		for (int i = expr.size() - 1; i >= 0; --i) {
			//call the Idfa class function Match that will path through the token dfa's node to find if it matches it
			expr[i].Match(str);
			if (expr[i].is_matching) {
				++matched;
				//if the match function reached an end node we store the information about the current string state
				if (expr[i].is_end) {
					curr = i;
					prec.second = str;
					prec.first = true;
					end_type = expr[i].id;
					token_name = expr[i].token_name;
				}
			}
		}
		//if no token matches we check if the last state (prec) was matching a token else it's an error because it's an unreconised token
		if (!matched) {
			if (prec.second.length() && prec.first) {
				if (end_type != "WHITESPACE") {
					buffer += token_name + " " + prec.second + "\n";
				}
				if (token_name == "LINE RETURN")
					++line;
				str.pop_back();
				//extra check for the int and float 0's
				if ((end_type == "SIGNED INT" || end_type == "FLOATING POINT NUMBER") && SEPARATORS.find(c) == std::string::npos
					|| (end_type == "FLOATING POINT NUMBER" && prec.second != str)) {
					std::cout << "ERROR ON LINE: " << line << "  >>  " << str << std::endl;
					return  84;
				}
				prec.second = "";
				token_name = "";
				str = c;
				//the last character isn't part of the reconised token as we didn't have any match so we reuse it instead of reading a new character
				goto rematch;
			}
			else if (str[1]) {
				str = &str[1];
				//get rid of the first character of the string if we didn't have any match and retry to match without reading a new character
				goto rematch;
			}
			else {
				std::cout << "ERROR ON LINE: " << line << std::endl;
				return  84;
			}
		}
	}
	//check the last token of the file
	if (matched) {
		if (prec.second.length() && prec.first) {
			buffer += token_name + " " + prec.second + "\n";
		}
	}
}