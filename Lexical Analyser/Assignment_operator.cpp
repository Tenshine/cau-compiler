#include "Assignment_operator.h"

//Creation of the DFA Graph of the ASSIGNMENT OPERATOR token
Assignment_operator::Assignment_operator() : Idfa("ASSIGNMENT OPERATOR") {
	head = new Node();

	auto q1 = new Node(true, "assign");

	head->addPair("=", q1);
}

Assignment_operator::~Assignment_operator() {}