#include "Definition_symbols.h"

//Creation of the DFA Graph of the DEFINITION SYMBOL token
Definition_symbols::Definition_symbols() : Idfa("DEFINITION SYMBOL") {
	head = new Node();

	auto q1 = new Node(true, "lbrace");
	auto q2 = new Node(true, "rbrace");

	head->addPair("{", q1);
	head->addPair("}", q2);
}

Definition_symbols::~Definition_symbols() {}