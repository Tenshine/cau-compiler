#include "Signed_int.h"

//Creation of the DFA Graph of the SIGNED INT token
Signed_int::Signed_int() : Idfa("SIGNED INT") {
	head = new Node();
	auto q1 = new Node();
	auto q2 = new Node(true, "num");
	auto q3 = new Node(true, "num");
	auto q4 = new Node(true, "num");

	head->addPair("-", q1);
	head->addPair("123456789", q2);
	head->addPair("0", q3);
	q1->addPair("123456789", q2);
	q2->addPair("0123456789", q4);
	q4->addPair("0123456789", q4);
}

Signed_int::~Signed_int() {}