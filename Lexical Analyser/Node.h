#pragma once

#include <string>
#include <vector>
#include <utility>

//definition the node class that will be used to create the dfa graph
class Node
{
public:
	Node();
	Node(bool, std::string);
	~Node();
	void addPair(std::string, Node *);

	std::vector<std::pair<std::string, Node *>> links;
	bool end;
	std::string name;
};


