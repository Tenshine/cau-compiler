#include "Float_number.h"

//Creation of the DFA Graph of the FLOATING POINT NUMBER token
Float_number::Float_number() : Idfa("FLOATING POINT NUMBER") {
	head = new Node();
	auto q1 = new Node();
	auto q2 = new Node();
	auto q3 = new Node();
	auto q4 = new Node();
	auto q5 = new Node(true, "vtype");
	auto q6 = new Node(true, "vtype");
	auto q7 = new Node();

	head->addPair("0", q1);
	head->addPair("-", q2);
	head->addPair("123456789", q3);

	q1->addPair(".", q4);

	q2->addPair("123456789", q3);
	q2->addPair("0", q1);

	q3->addPair("0123456789", q3);
	q3->addPair(".", q4);

	q4->addPair("0123456789", q5);

	q5->addPair("123456789", q6);
	q5->addPair("0", q7);

	q6->addPair("123456789", q6);
	q6->addPair("0", q7);

	q7->addPair("123456789", q6);
	q7->addPair("0", q7);
}

Float_number::~Float_number() {}