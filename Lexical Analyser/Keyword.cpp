#include "Keyword.h"

//Creation of the DFA Graph of the KEYWORD token
Keyword::Keyword() : Idfa("KEYWORD") {
	head = new Node();

	auto q1 = new Node();
	auto q2 = new Node(true, "if");

	auto q3 = new Node();
	auto q4 = new Node();
	auto q5 = new Node();
	auto q6 = new Node(true, "else");
	
	auto q7 = new Node();
	auto q8 = new Node();
	auto q9 = new Node();
	auto q10 = new Node();
	auto q11 = new Node(true, "while");

	auto q12 = new Node();
	auto q13 = new Node();
	auto q14 = new Node(true, "for");

	auto q15 = new Node();
	auto q16 = new Node();
	auto q17 = new Node();
	auto q18 = new Node();
	auto q19 = new Node();
	auto q20 = new Node(true, "return");

	head->addPair("i", q1);
	q1->addPair("f", q2);
		
	head->addPair("e", q3);
	q3->addPair("l", q4);
	q4->addPair("s", q5);
	q5->addPair("e", q6);

	head->addPair("w", q7);
	q7->addPair("h", q8);
	q8->addPair("i", q9);
	q9->addPair("l", q10);
	q10->addPair("e", q11);

	head->addPair("f", q12);
	q12->addPair("o", q13);
	q13->addPair("r", q14);

	head->addPair("r", q15);
	q15->addPair("e", q16);
	q16->addPair("t", q17);
	q17->addPair("u", q18);
	q18->addPair("r", q19);
	q19->addPair("n", q20);
}

Keyword::~Keyword() {}