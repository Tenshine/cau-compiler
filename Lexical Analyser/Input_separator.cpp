#include "Input_separator.h"

//Creation of the DFA Graph of the INPUT SEPARATOR token
Input_separator::Input_separator(): Idfa("INPUT SEPARATOR") {
	head = new Node();

	auto q1 = new Node(true, "comma");

	head->addPair(",", q1);
}

Input_separator::~Input_separator() {}