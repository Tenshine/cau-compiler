#include "Identifier.h"

//Creation of the DFA Graph of the IDENTIFIER token
Identifier::Identifier() : Idfa("IDENTIFIER") {
	head = new Node();

	auto q1_1 = new Node(true, "id");
	auto q2_1 = new Node(true, "id");

	head->addPair("_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", q1_1);
	q1_1->addPair("_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", q2_1);
	q2_1->addPair("_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", q2_1);
}

Identifier::~Identifier() {}