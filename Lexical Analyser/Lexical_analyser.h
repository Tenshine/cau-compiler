#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <utility>
#include "Variable_type.h"
#include "Boolean_string.h"
#include "Expr_string.h"
#include "Keyword.h"
#include "Arithmetic_operators.h"
#include "Bitwise_operators.h"
#include "Assignment_operator.h"
#include "Comparison_operators.h"
#include "Term_statement.h"
#include "Definition_symbols.h"
#include "Indication_symbols.h"
#include "Input_separator.h"
#include "Identifier.h"
#include "Signed_int.h"
#include "Float_number.h"
#include "Whitespaces.h"

class Lexical_analyser
{
public:
	Lexical_analyser(std::string);
	~Lexical_analyser();
    int Analyse();
    void Open_file(std::string _file);

private:
    std::vector<Idfa> expr;
	const std::string SEPARATORS = " ,;{}()=+*/-<>!&|\t\n";
    std::string str = "";
    std::string end_type = "";
    std::string token_name = "";
    std::pair<bool, std::string> prec = {false, ""};
    char c = 0;
    int matched = 0;
    int curr = 0;

public:
    int line = 1;
    std::ifstream file;
    std::string buffer = "";
};

