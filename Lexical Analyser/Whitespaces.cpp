#include "Whitespaces.h"

//Creation of the DFA Graph of the WITESPACE token
Whitespaces::Whitespaces() : Idfa("WHITESPACE") {
	head = new Node();

	auto q1 = new Node(true, "WHITESPACE");
	auto q2 = new Node(true, "WHITESPACE");
	auto q3 = new Node(true, "LINE_RETURN");

	head->addPair(" ", q1);
	head->addPair("\t", q2);
	head->addPair("\n", q3);
}

Whitespaces::~Whitespaces() {}