#include "Variable_type.h"

//Creation of the DFA Graph of the VARIABLE TYPE token
Variable_type::Variable_type() : Idfa("VARIABLE TYPE")
{
	head = new Node();

	auto q1 = new Node();
	auto q2 = new Node();
	auto q3 = new Node();
	auto q4 = new Node();
	auto q5 = new Node(true, "vtype");

	auto q6 = new Node();
	auto q7 = new Node();
	auto q8 = new Node();
	auto q9 = new Node(true, "vtype");

	auto q10 = new Node();
	auto q11 = new Node();
	auto q12 = new Node();
	auto q13 = new Node(true, "vtype");

	auto q14 = new Node();
	auto q15 = new Node();
	auto q16 = new Node(true, "vtype");

	head->addPair("f", q1);
	q1->addPair("l", q2);
	q2->addPair("o", q3);
	q3->addPair("a", q4);
	q4->addPair("t", q5);

	head->addPair("b", q6);
	q6->addPair("o", q7);
	q7->addPair("o", q8);
	q8->addPair("l", q9);

	head->addPair("c", q10);
	q10->addPair("h", q11);
	q11->addPair("a", q12);
	q12->addPair("r", q13);

	head->addPair("i", q14);
	q14->addPair("n", q15);
	q15->addPair("t", q16);
}

Variable_type::~Variable_type()
{
}
