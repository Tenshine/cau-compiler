#include "Term_statement.h"

//Creation of the DFA Graph of the TERMINATION STATEMENT token
Term_statement::Term_statement() : Idfa("TERMINATION STATEMENT") {
	head = new Node();
	auto q1 = new Node(true, "semi");

	head->addPair(";", q1);
}

Term_statement::~Term_statement() {}