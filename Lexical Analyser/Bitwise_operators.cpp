#include "Bitwise_operators.h"

//Creation of the DFA Graph of the BITWISE OPERATOR token
Bitwise_operators::Bitwise_operators() : Idfa("BITWISE OPERATOR") {
	head = new Node();

	auto q1 = new Node();
	auto q2 = new Node(true, "BITWISE_LEFT_SHIFT");

	auto q3 = new Node();
	auto q4 = new Node(true, "BITWISE_RIGHT_SHIFT");

	auto q5 = new Node(true, "BITWISE_AND");

	auto q6 = new Node(true, "BITWISE_OR");

	head->addPair("<", q1);
	q1->addPair("<", q2);

	head->addPair(">", q3);
	q3->addPair(">", q4);
	
	head->addPair("&", q5);
	
	head->addPair("|", q6);
}

Bitwise_operators::~Bitwise_operators() {}