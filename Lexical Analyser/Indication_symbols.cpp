#include "Indication_symbols.h"

//Creation of the DFA Graph of the INDICATION SYMBOL token
Indication_symbols::Indication_symbols() : Idfa("INDICATION SYMBOL") {
	head = new Node();

	auto q1 = new Node(true, "lparen");
	auto q2 = new Node(true, "rparen");

	head->addPair("(", q1);
	head->addPair(")", q2);
}

Indication_symbols::~Indication_symbols() {}