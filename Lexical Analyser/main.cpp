// Lexical Analyser.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include "Lexical_analyser.h"

int main(int ac, char** av)
{
	//check if the program has a paramater if not print error message and exit
	if (ac < 2) {
		std::cout << "Missing parameter" << std::endl;
		return 84;
	}
	std::string filename = av[1];
	//create an instance of out Lexical_analyser class that will perform the and analyse on the given file
	auto analyser = new Lexical_analyser(filename);
	size_t lastindex = filename.find_last_of(".");
	std::string output = filename;

	//get rid of file extension if it has one to get the output filename
	if (lastindex != std::string::npos)
		output = filename.substr(0, lastindex);
	output += ".out";
	//call Analyse function that will read the whole file and fill the buffer public string with a table of symbols
	analyser->Analyse();

	//open outpur file
	std::ofstream ofs(output, std::ofstream::out);
	//write the content of the buffer after the analyse in the output file
	ofs << analyser->buffer;
	ofs.close();
	//print outpur file name
	std::cout << output << std::endl;
	return 0;
}