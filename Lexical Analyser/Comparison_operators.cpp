#include "Comparison_operators.h"

//Creation of the DFA Graph of the COMPARISON OPERATOR token
Comparison_operators::Comparison_operators() : Idfa("COMPARISON OPERATOR") {
	head = new Node();

	auto q1 = new Node(true, "comp");

	auto q2 = new Node(true, "comp");

	auto q3 = new Node();
	auto q4 = new Node(true, "comp");

	auto q5 = new Node();
	auto q6 = new Node(true, "comp");

	auto q7 = new Node(true, "comp");
	auto q8 = new Node(true, "comp");

	head->addPair("<", q1);
	q1->addPair("=", q7);

	head->addPair(">", q2);
	q2->addPair("=", q8);

	head->addPair("=", q3);
	q3->addPair("=", q4);

	head->addPair("!", q5);
	q5->addPair("=", q6);

}

Comparison_operators::~Comparison_operators() {}