#include "Boolean_string.h"

//Creation of the DFA Graph of the BOOLEAN STRING token
Boolean_string::Boolean_string() : Idfa("BOOLEAN STRING") {
	head = new Node();
	auto q1 = new Node();
	auto q2 = new Node();
	auto q3 = new Node();
	auto q4 = new Node(true, "vtype");

	head->addPair("t", q1);
	q1->addPair("r", q2);
	q2->addPair("u", q3);
	q3->addPair("e", q4);

	auto q5 = new Node();
	auto q6 = new Node();
	auto q7 = new Node();
	auto q8 = new Node();
	auto q9 = new Node(true, "vtype");

	head->addPair("f", q5);
	q5->addPair("a", q6);
	q6->addPair("l", q7);
	q7->addPair("s", q8);
	q8->addPair("e", q9);
}

Boolean_string::~Boolean_string() {}